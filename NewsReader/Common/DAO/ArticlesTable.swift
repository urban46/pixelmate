//
//  ArticlesTable.swift
//  NewsReader
//
//  Created by patrikurban on 18/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation
import RealmSwift

class ArticlesTable: Object {
    @objc dynamic var id = 0
    @objc dynamic var title = ""
    @objc dynamic var author = ""
    @objc dynamic var content = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
