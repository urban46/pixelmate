//
//  ApiCallService.swift
//  NewsReader
//
//  Created by patrikurban on 17/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation
import Alamofire


class ApiCallService {
    
    static func getSourcesData(handler: @escaping ([Sources]) -> Void) {
        
        let url = "https://newsapi.org/v1/sources"
        getDataBase(url: url, keyJson: "sources", handler: { data in
            
            if let values = data as? [Dictionary<String,Any>] {
                var listOfArticles = [Sources]()
                for item in values {
                    listOfArticles.append(Sources(dictionary: item))
                }
                handler(listOfArticles)
                return
            }
            handler([Sources]())
        })
    }
    
    static func getListNews(idSource: String, handler: @escaping ([NewsFeed]) -> Void) {
        
        let url = "https://newsapi.org/v2/top-headlines?sources=\(idSource)&apiKey=a2c307b0f09e42a3bdfe4adce969a096"
        getDataBase(url: url, keyJson: "articles", handler:  { data in
            
                if let values = data as? [Dictionary<String,Any>] {
                    var listOfArticles = [NewsFeed]()
                    for item in values {
                        listOfArticles.append(NewsFeed(dictionary: item))
                    }
                    handler(listOfArticles)
                    return
                }
                handler([NewsFeed]())
        })
    }
    
    private static func getDataBase(url: String, keyJson: String, handler: @escaping ([AnyObject]) -> Void) {
        
        let urlValue = URL(string: url)
        Alamofire.request(urlValue!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON  { response in
            
            switch(response.result)
            {
                case .success( _):
                    if let json = response.result.value, let jsonDictionary = json as? NSDictionary, let sources =  jsonDictionary[keyJson] as? [NSDictionary] {
                        handler(sources)
                        return
                    }
                    handler([AnyObject]())
                case .failure( _):
                    handler([AnyObject]())
            }
        }
    }
}
