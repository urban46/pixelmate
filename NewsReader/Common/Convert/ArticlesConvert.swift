//
//  ArticlesConvert.swift
//  NewsReader
//
//  Created by patrikurban on 18/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation

class ArticlesConvert {
    static func toTable(article: Articles) -> ArticlesTable {
        let articles = ArticlesTable()
        
        articles.id = article.id ?? 0
        articles.author = article.author
        articles.title = article.title
        articles.content = article.content
        
        return articles
    }
    
    static func toModel(articleTable: ArticlesTable) -> Articles {
        let articles = Articles()
        
        articles.id = articleTable.id
        articles.author = articleTable.author
        articles.title = articleTable.title
        articles.content = articleTable.content
        
        return articles
    }
}
