//
//  UITableExtension.swift
//  NewsReader
//
//  Created by patrikurban on 18/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func noDataLabel(dataCount: Int, message: String) {
        
        if dataCount > 0
        {
            self.separatorStyle = .singleLine
            self.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
            noDataLabel.text          = message
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            self.backgroundView  = noDataLabel
            self.separatorStyle  = .none
        }
    }
}
