//
//  Articles.swift
//  NewsReader
//
//  Created by patrikurban on 18/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation

class Articles {
    var content: String = ""
    var author: String = ""
    var title: String = ""
    var id: Int?
}
