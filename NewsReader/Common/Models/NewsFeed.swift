//
//  NewsFeed.swift
//  NewsReader
//
//  Created by patrikurban on 17/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation

class NewsFeed : Decodable {
    var author: String = ""
    var title: String = ""
    var url: String = ""
    var publishedAt: String = ""
    var content: String = ""
    
    init (dictionary: Dictionary<String, Any>) {
        author = dictionary["author"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
        url = dictionary["url"] as? String ?? ""
        publishedAt = dictionary["publishedAt"] as? String ?? ""
        content = dictionary["content"] as? String ?? ""
    }
}
