//
//  Sources.swift
//  NewsReader
//
//  Created by patrikurban on 17/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//
import Foundation

class Sources {
    var category: String = ""
    var country: String = ""
    var description: String = ""
    var id: String = ""
    var language: String = ""
    var name: String = ""
    var sortBysAvailable: [String] = [String]()
    var url: String = ""
    
    init (dictionary: Dictionary<String, Any>) {
        category = dictionary["category"] as? String ?? ""
        country = dictionary["country"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        id = dictionary["id"] as? String ?? ""
        language = dictionary["language"] as? String ?? ""
        name = dictionary["name"] as? String ?? ""
        url = dictionary["url"] as? String ?? ""
        sortBysAvailable = dictionary["name"] as? [String] ?? [String]()
    }
}
