//
//  ViewController.swift
//  NewsReader
//
//  Created by patrikurban on 16/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import UIKit

protocol OnlineArticlesDelegate {
    func reloadOnlineArticles(sourcesId: String)
}

class OnlineArticlesController: UIViewController {

    var btnCategory: UIBarButtonItem!
    @IBOutlet weak var table: UITableView!
    
    var articlesData = [NewsFeed]() {
        didSet{
            table.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDidLoadSet()
    }
    
    private func viewDidLoadSet() {
        
        table.dataSource = self
        table.delegate = self
        
        table.separatorColor = UIColor.gray
        table.layoutMargins = UIEdgeInsets.zero
        table.separatorInset = UIEdgeInsets.zero
    
        let uiButton = UIBarButtonItem(barButtonSystemItem: .organize, target: self, action: #selector(btnCategoryClick))
        tabBarController?.navigationItem.leftBarButtonItem = uiButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.navigationItem.title = "News"
        tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = true
    }
    
    @objc func btnCategoryClick() {
        performSegue(withIdentifier: "sqCategorySeletor", sender: nil)
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let segueIdent = segue.identifier, segueIdent == "sqCategorySeletor",
            let nc = segue.destination as? UINavigationController,
            let dest = nc.viewControllers.first as? SourcesViewController
        {
            dest.vcDelegate = self
        }
        
        if let segueIdent = segue.identifier, segueIdent == "sArticleDetails",
            let vc = segue.destination as? UINavigationController,
            let dest = vc.viewControllers.first as? DetailController, let row = sender as? Int
        {
            dest.newsFeed = articlesData[row]
        }
    }
}

extension OnlineArticlesController : OnlineArticlesDelegate {
    func reloadOnlineArticles(sourcesId: String) {
        ApiCallService.getListNews(idSource: sourcesId, handler: { articles in
            self.articlesData = articles
        })
    }
}

extension OnlineArticlesController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let numOfSections = articlesData.count
        tableView.noDataLabel(dataCount: numOfSections, message: "Please select your surces")
        
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "articlesCell") as! ArticlesCell
        cell.lblTitle.text = articlesData[indexPath.row].title
        cell.lblAuthor.text = articlesData[indexPath.row].url
        return cell
    }
}

extension OnlineArticlesController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "sArticleDetails", sender: indexPath.row)
    }
}
