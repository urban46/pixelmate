//
//  CategoryViewController.swift
//  NewsReader
//
//  Created by patrikurban on 17/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import UIKit

class SourcesViewController: UIViewController {
    
    @IBOutlet var table: UITableView!
    var vcDelegate: OnlineArticlesDelegate?
    var sourcesProvider: ISourcesProvider = SourcesProvider()
    var sourcesData: [Sources] = [Sources]() {
        didSet{
            table.reloadData()
        }
    }
    var sourcesId: String?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        table.allowsMultipleSelection = false
        table.separatorColor = UIColor.gray
        table.layoutMargins = UIEdgeInsets.zero
        table.separatorInset = UIEdgeInsets.zero
        
        sourcesProvider.getSources(handler: { data in
            self.sourcesData = data
        })
    }

    // MARK: - Table view data source

    @IBAction func btnCancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSaveSelector(_ sender: Any) {
    
        if let sourcesKey = sourcesId {
            vcDelegate?.reloadOnlineArticles(sourcesId: sourcesKey)
        }
        dismiss(animated: true, completion: nil)
    }
}

extension  SourcesViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numOfSections = sourcesData.count
        tableView.noDataLabel(dataCount: numOfSections, message: "List is refreshed in background")
        
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdent", for: indexPath)
        cell.textLabel?.text = sourcesData[indexPath.row].name
        return cell
    }
}

extension SourcesViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sourcesId = sourcesData[indexPath.row].id
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                cell.accessoryType = .none
            } else {
                cell.accessoryType = .checkmark
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        cell.accessoryType = .none
    }
}
