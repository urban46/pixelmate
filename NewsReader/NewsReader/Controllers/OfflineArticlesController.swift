//
//  OfflineArticlesController.swift
//  NewsReader
//
//  Created by patrikurban on 18/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import UIKit

class OfflineArticlesController: UIViewController {

    @IBOutlet weak var table: UITableView!
    private var provider = ArticlesProvider()
    private var sortedSwitch = false
    
    var data = [Articles]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        table.dataSource = self
        table.delegate = self
        
        table.separatorColor = UIColor.gray
        table.layoutMargins = UIEdgeInsets.zero
        table.separatorInset = UIEdgeInsets.zero
        
        let uiButton = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(btnSortClick))
        tabBarController?.navigationItem.rightBarButtonItem = uiButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tabBarController?.navigationItem.title = "Booksmark"
        tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = false
        
        data = provider.getAllFromDb()
        table.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let segueIdent = segue.identifier, segueIdent == "sArticleDetails",
            let vc = segue.destination as? UINavigationController,
            let dest = vc.viewControllers.first as? DetailController, let row = sender as? Int
        {
            dest.offlineDb = data[row]
        }
    }
    
    @objc func btnSortClick(){
        
        if sortedSwitch {
            data = data.sorted() { $0.author > $1.author }
            sortedSwitch = false
        } else {
            data = data.sorted() { $0.author < $1.author }
            sortedSwitch = true
        }
        
        table.reloadData()
    }
}

extension OfflineArticlesController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "sArticleDetails", sender: indexPath.row)
    }
}

extension OfflineArticlesController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numOfSections = data.count
        tableView.noDataLabel(dataCount: numOfSections, message: "No saved data")
        
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let uicell = UITableViewCell()
        uicell.textLabel?.text = data[indexPath.row].title
        return uicell
    }
}
