//
//  DetailController.swift
//  NewsReader
//
//  Created by patrikurban on 17/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import UIKit
import WebKit

class DetailController: UIViewController {
    
    var newsFeed: NewsFeed?
    var offlineDb: Articles?
    var provider = ArticlesProvider()
    var btnAction: UIBarButtonItem!
    @IBOutlet weak var web: WKWebView!
    
    private var tempStorage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        web.navigationDelegate = self
        
        setView()
        load()
    }
    
    private func setView()
    {
        if offlineDb != nil {
            btnAction = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(btnTrashClick))
        } else {
            btnAction = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(btnAddClick))
            btnAction.isEnabled = false
        }
        navigationItem.rightBarButtonItem = btnAction
    }
    
    private func load()
    {
        if let model = offlineDb {
            web.loadHTMLString("\(model.content)", baseURL: nil)
        } else if let feed = newsFeed{
            let url = URL(string: feed.url)!
            web.load(URLRequest(url: url))
            web.allowsBackForwardNavigationGestures = true
        }
    }
    
    @IBAction func cancelDetail(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func btnTrashClick(){
        if let model = offlineDb {
            provider.delete(data: model)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func btnAddClick(){
        
        if let contentValue = tempStorage,
            let feed = newsFeed {
            
            let model = Articles()
            model.author = feed.author
            model.title = feed.title
            model.content = contentValue
            provider.insertToDb(data: model)
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension DetailController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        web.evaluateJavaScript("document.documentElement.outerHTML.toString()", completionHandler: { html, error in
            self.tempStorage = "\(html!)"
            self.btnAction.isEnabled = true
        })
    }
}
