//
//  ArticlesCell.swift
//  NewsReader
//
//  Created by patrikurban on 18/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import UIKit

class ArticlesCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
