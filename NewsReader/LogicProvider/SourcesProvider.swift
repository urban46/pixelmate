//
//  CategoryProvider.swift
//  NewsReader
//
//  Created by patrikurban on 17/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation

protocol ISourcesProvider {
    func getSources(handler: @escaping ([Sources]) -> Void)
}

class SourcesProvider: ISourcesProvider {
    func getSources(handler: @escaping ([Sources]) -> Void) {
        ApiCallService.getSourcesData(handler: handler)
    }
}
