//
//  ArticlesProvider.swift
//  NewsReader
//
//  Created by patrikurban on 17/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation

protocol IArticles {
    func getArticles(sourcesId: String,handler: @escaping ([NewsFeed]) -> Void)
    func getAllFromDb() -> [Articles]
    func insertToDb(data: Articles)
    func delete(data: Articles)
}

class ArticlesProvider: IArticles {
    
    let dbRepo: IArticlesRepo = ArticlesRepo()
    
    func getArticles(sourcesId: String, handler: @escaping ([NewsFeed]) -> Void) {
        ApiCallService.getListNews(idSource: sourcesId, handler: handler)
    }
    
    func getAllFromDb() -> [Articles] {
        var result = [Articles]()
        for item in dbRepo.getAll() {
            result.append(ArticlesConvert.toModel(articleTable: item))
        }
        return result
    }
    
    func insertToDb(data: Articles) {
        dbRepo.insert(data: ArticlesConvert.toTable(article: data))
    }
    
    func delete(data: Articles) {
        if let id = data.id {
            dbRepo.delete(data: id)
        }
    }
}
