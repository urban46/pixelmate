//
//  File.swift
//  NewsReader
//
//  Created by patrikurban on 18/02/2019.
//  Copyright © 2019 Patrik Urban. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

protocol IArticlesRepo {
    func getAll() -> [ArticlesTable]
    func insert(data: ArticlesTable)
    func delete(data: Int)
}

class ArticlesRepo : IArticlesRepo {
    func getAll() -> [ArticlesTable] {
        let realm = try! Realm()
        return Array(realm.objects(ArticlesTable.self))
    }
    
    func insert(data: ArticlesTable) {
        let realm = try! Realm()
        
        data.id = incrementId()
        
        try! realm.write {
            realm.add(data)
        }
    }
    
    func delete(data: Int) {
        let realm = try! Realm()
        let article = realm.object(ofType: ArticlesTable.self, forPrimaryKey: data)
        
        try! realm.write {
            realm.delete(article!)
        }
    }
    
    private func incrementId() -> Int {
        let realm = try! Realm()
        return (realm.objects(ArticlesTable.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
